# README for Isabelle Listings

The `isabelle-listings.sty` provides a colored listings definition for the 
Isabelle language.

Note that your compilation time may increase a bit when using this package

# Usage
 1. add `isabelle-listings.sty` to your project root
 2. use the necessary packages: `listingsutf8, amsmath, amssymb, amstext, wasysym`
 3. add `\usepackage{isabelle-listing}` before `\begin{document}`
 4. setup the listings package via `\lstset`
    * do not forget to set the language to Isabelle
 5. highlight sourcecode via `\lstinputlisting, \lstinline` or  the `lstlisting` environment
 
# Disclaimer
The provided package file is only an approximation to the actual jEdit highlight and not complete, 
feel free to add additional literals if necessary.
	
# Example lstset
```latex
\lstset{%
    % General design
    aboveskip=10pt,
    belowskip=10pt,
    % Code
    language={Isabelle},
    breaklines=true, 
    breakatwhitespace=true, 
    mathescape=true,
}
```

